<?php
if(!(php_sapi_name() === 'cli')){
    die("not cli, exit");
}

include ".db.php";

error_reporting(E_ALL);
ini_set('memory_limit', '2512M');
ini_set('display_errors', 'On');
ini_set('max_execution_time', '0');
set_time_limit(0);
ob_implicit_flush();
include_once '.db.php';
require 'vendor/autoload.php';

$root_local_images_path="/var/www/back_for_invest_cabinet_test";
$sFile="general_info.xlsx";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
$oReader = new Xlsx();
$oSpreadsheet = $oReader->load($sFile);
$oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();
$indexNewXlsxRows=0;
for ($iRow = 1; $iRow <= $oCells->getHighestRow(); $iRow++){
 for ($iCol = 'A'; $iCol <= 'ZZ'; $iCol++){
	 
  $oCell = $oCells->get($iCol.$iRow);
  if($oCell){
    ///////////////////
    ///load data///////
    //////////////////
    if($iRow>1){//skip first row with headers
    	 //echo "iCol=".$iCol." ; iRow=".$iRow." "."<br>\n";
        if($iCol=='A'){
			$POLICY_ID=$oCell->getValue();
        }
        if($iCol=='B'){
			$POLICY_NO=$oCell->getValue();
        }
        if($iCol=='C'){
			$STATUS=$oCell->getValue();
    	}
        if($iCol=='D'){
			$CURRENCY=$oCell->getValue();
        }
        if($iCol=='E'){
			$CURRENCY_POLICY=$oCell->getValue();
    	}
    	if($iCol=='F'){
			$SEX_TEXT=$oCell->getValue();
    	}
    	if($iCol=='G'){
			$CLIENT_NAME=$oCell->getValue();
    	}
    	if($iCol=='H'){
			$REPORT_DATE=$oCell->getValue();
    	}
    	if($iCol=='I'){
			$INSR_BEGIN=$oCell->getValue();
    	}
        if($iCol=='J'){
			$INSR_END=$oCell->getValue();
        }
        if($iCol=='K'){
			$INDEX_NO=$oCell->getValue();
        }
        if($iCol=='L'){
			$INDEX_LEVEL=$oCell->getValue();
        }
        if($iCol=='M'){
			$INDEX_LEVEL_RND=$oCell->getValue();
        }
        if($iCol=='N'){
			$INDEX_LEVEL_TEXT=$oCell->getValue();
        }
        if($iCol=='O'){
			$INDEX_GROWTH=$oCell->getValue();
        }
        if($iCol=='P'){
			$INDEX_GROWTH_SIGNED=$oCell->getValue();
        }
        if($iCol=='Q'){
			$CURRENCY_DATE=$oCell->getValue();
        }
        if($iCol=='R'){
			$CURRENCY_VALUE=$oCell->getValue();
        }
        if($iCol=='S'){
			$CURRENCY_GROWTH=$oCell->getValue();
        }
        if($iCol=='T'){
			$CURRENCY_GROWTH_SIGNED=$oCell->getValue();
        }
        if($iCol=='U'){
			$SURRENDER_VALUE=$oCell->getValue();
        }
        if($iCol=='V'){
			$ORIG_SURRENDER_VALUE=$oCell->getValue();
        }
        if($iCol=='W'){
			$PARTICIPATION_RATE=$oCell->getValue();
        }
        if($iCol=='X'){
			$PARTICIPATION_PERCENTAGE=$oCell->getValue();
        }
        if($iCol=='Y'){
			$INVESTMENT_GROWTH=$oCell->getValue();
        }
        if($iCol=='Z'){
			$INVESTMENT_INCOME=$oCell->getValue();
        }
        if($iCol=='AA'){
			$STRATEGY_NAME=$oCell->getValue();
        }
        if($iCol=='AB'){
			$PREMIUM=$oCell->getValue();
        }
        if($iCol=='AC'){
			$INSURANCE_PROGRAM=$oCell->getValue();
        }
        if($iCol=='AD'){
			$INVESTMENT_DATE=$oCell->getValue();
        }
        if($iCol=='AE'){
			$INDEX_STRIKE_VALUE=$oCell->getValue();
        }
        if($iCol=='AF'){
			$INDEX_STRIKE_RND=$oCell->getValue();
        }
        if($iCol=='AG'){
			$INDEX_STRIKE_TEXT=$oCell->getValue();
        }
        if($iCol=='AH'){
			$INITIAL_CURRENCY_RATE=$oCell->getValue();
        }
        if($iCol=='AI'){
			$INDEX_DESCR=$oCell->getValue();
        }
        if($iCol=='AJ'){
			$GUARANTEED_VALUE=$oCell->getValue();
        }
        if($iCol=='AK'){
			$INDEX_MIN_LEVEL=$oCell->getValue();
        }
        if($iCol=='AL'){
			$FILE_LABEL=$oCell->getValue();
        }
        if($iCol=='AM'){
			$REAL_REPORT_DATE=$oCell->getValue();
        }
        if($iCol=='AN'){
			$INSR_BEGIN_MONTH=$oCell->getValue();
        }
        if($iCol=='AO'){
			$INSR_END_MONTH=$oCell->getValue();
        }
        if($iCol=='AP'){
			$GRAPH_END_MONTH=$oCell->getValue();
        }
        if($iCol=='AQ'){
            $GRAPH_START_MONTH=$oCell->getValue();
        	updateIN_DB_main_table($mysqli,$POLICY_ID, $POLICY_NO, $STATUS, $CURRENCY, $CURRENCY_POLICY, $SEX_TEXT, $CLIENT_NAME, $REPORT_DATE, $INSR_BEGIN, $INSR_END, $INDEX_NO, $INDEX_LEVEL, $INDEX_LEVEL_RND, $INDEX_LEVEL_TEXT, $INDEX_GROWTH, $INDEX_GROWTH_SIGNED, $CURRENCY_DATE, $CURRENCY_VALUE, $CURRENCY_GROWTH, $CURRENCY_GROWTH_SIGNED, $SURRENDER_VALUE, $ORIG_SURRENDER_VALUE, $PARTICIPATION_RATE, $PARTICIPATION_PERCENTAGE, $INVESTMENT_GROWTH, $INVESTMENT_INCOME, $STRATEGY_NAME, $PREMIUM, $INSURANCE_PROGRAM, $INVESTMENT_DATE, $INDEX_STRIKE_VALUE, $INDEX_STRIKE_RND, $INDEX_STRIKE_TEXT, $INITIAL_CURRENCY_RATE, $INDEX_DESCR, $GUARANTEED_VALUE, $INDEX_MIN_LEVEL, $FILE_LABEL, $REAL_REPORT_DATE, $INSR_BEGIN_MONTH, $INSR_END_MONTH, $GRAPH_END_MONTH, $GRAPH_START_MONTH);
    	}
    }//check if it first row
  }//end of [if oCell]
 }
}

?>
