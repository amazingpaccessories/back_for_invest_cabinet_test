CREATE TABLE general_data(
		id INT AUTO_INCREMENT PRIMARY KEY,
		POLICY_ID INT NOT NULL, 
		POLICY_NO VARCHAR(150) NOT NULL, 
		STATUS VARCHAR(100) NOT NULL, 
		CURRENCY VARCHAR(20) NOT NULL,
		CURRENCY_POLICY VARCHAR(10) NOT NULL,
		SEX_TEXT VARCHAR(20) NOT NULL,
		CLIENT_NAME VARCHAR(250) NOT NULL,
		REPORT_DATE DATETIME NOT NULL,
		INSR_BEGIN DATETIME NOT NULL,
		INSR_END DATETIME NOT NULL,
		INDEX_NO VARCHAR(100) NOT NULL, 
		INDEX_LEVEL DECIMAL(16,3) NOT NULL, 
		INDEX_LEVEL_RND INT NOT NULL,
		INDEX_LEVEL_TEXT VARCHAR(100) NOT NULL,
		INDEX_GROWTH DECIMAL(6,2) NOT NULL,
		INDEX_GROWTH_SIGNED VARCHAR(50) NOT NULL,
		CURRENCY_DATE DATETIME NOT NULL,
		CURRENCY_VALUE DECIMAL(16,3) NOT NULL,
		CURRENCY_GROWTH DECIMAL(6,2) NOT NULL,
		CURRENCY_GROWTH_SIGNED VARCHAR(50) NOT NULL,
		SURRENDER_VALUE DECIMAL(16,3) NOT NULL,
		ORIG_SURRENDER_VALUE INT NOT NULL,
		PARTICIPATION_RATE DECIMAL(6,2) NOT NULL,
		PARTICIPATION_PERCENTAGE INT NOT NULL,
		INVESTMENT_GROWTH DECIMAL(6,2) NOT NULL,
		INVESTMENT_INCOME DECIMAL(16,2) NOT NULL,
		STRATEGY_NAME VARCHAR(250) NOT NULL,
		PREMIUM INT NOT NULL,
		INSURANCE_PROGRAM VARCHAR(50) NOT NULL,
		INVESTMENT_DATE DATETIME NOT NULL,
		INDEX_STRIKE_VALUE DECIMAL(16,3) NOT NULL,
		INDEX_STRIKE_RND INT NOT NULL,
		INDEX_STRIKE_TEXT VARCHAR(100) NOT NULL,
		INITIAL_CURRENCY_RATE DECIMAL(16,2) NOT NULL,
		INDEX_DESCR TEXT NOT NULL,
		GUARANTEED_VALUE INT NOT NULL,
		INDEX_MIN_LEVEL DECIMAL(16,3) NOT NULL,
		FILE_LABEL TEXT NOT NULL,
		REAL_REPORT_DATE DATETIME NOT NULL,
		INSR_BEGIN_MONTH VARCHAR(50) NOT NULL,
		INSR_END_MONTH VARCHAR(50) NOT NULL,
		GRAPH_END_MONTH VARCHAR(50) NOT NULL,
		GRAPH_START_MONTH VARCHAR(50) NOT NULL
)ENGINE=INNODB;
