<?php
if(!(php_sapi_name() === 'cli')){
    die("not cli, exit");
}

include ".db.php";

error_reporting(E_ALL);
ini_set('memory_limit', '3512M');
ini_set('display_errors', 'On');
ini_set('max_execution_time', '0');
set_time_limit(0);
ob_implicit_flush();
include_once '.db.php';
require 'vendor/autoload.php';

//get currency from cbr.ru
	$arrayOfCurrencyValues=[];
	$arrayOfCurrencyValues=getCurrentCurrencyValues();

$root_local_path="/var/www/back_for_invest_cabinet_test";

$sFile="dailyUpdateSGIXCE14.xlsx";

$url="https://sgi.sgmarkets.com/rest_en/excel?ticker=SGIXCE14";

$command="wget --no-check-certificate -O ".$root_local_path."/".$sFile." ".$url;
//exec($command);
sleep(5);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
$oReader = new Xlsx();
$oSpreadsheet = $oReader->load($root_local_path."/".$sFile);
$oCells = $oSpreadsheet->getActiveSheet()->getCellCollection();
$indexNewXlsxRows=0;
for ($iRow = 1; $iRow <= $oCells->getHighestRow(); $iRow++){
 for ($iCol = 'A'; $iCol <= 'D'; $iCol++){
	 
  $oCell = $oCells->get($iCol.$iRow);
  if($oCell){
    ///////////////////
    ///load data///////
    //////////////////
    if($iRow>1){//skip first row with headers
    	 //echo "iCol=".$iCol." ; iRow=".$iRow." "."<br>\n";
        if($iCol=='A'){
			//Code BLG ( INDEX_NO in base table)
			$INDEX_NO=$oCell->getValue();
        }
        if($iCol=='B'){
			//Price Date ( REPORT_DATE in base table )
			$REPORT_DATE=$oCell->getValue();
        }
        if($iCol=='C'){
			//Currency ( CURRENCY_POLICY in base table )
			$CURRENCY_POLICY=$oCell->getValue();
    	}
        if($iCol=='D'){
			//Index Level ( INDEX_LEVEL in base table )
			$INDEX_LEVEL=$oCell->getValue();
        }
    }//check if it first row
  }//end of [if oCell]
 }
}
if((!empty($INDEX_NO))AND(!empty($REPORT_DATE))AND(!empty($CURRENCY_POLICY))AND(!empty($INDEX_LEVEL))){
	echo "INDEX_NO=".$INDEX_NO.", REPORT_DATE=".$REPORT_DATE.", CURRENCY_POLICY=".$CURRENCY_POLICY.", INDEX_LEVEL=".$INDEX_LEVEL."\n";
	updateIN_DB($mysqli, $INDEX_NO, $REPORT_DATE, $CURRENCY_POLICY, $INDEX_LEVEL, $arrayOfCurrencyValues);
}
echo "end of update for SGIXCE14";

?>
