<?php
	define('DBHST', 'localhost');
	define('DBPRT', 3306);
	define('DBUSR', 'test_ros_bank_user');
	define('DBPSWD', '#oPw0123!');
	define('DBNM', 'rosbank_test');
	$mysqli = new mysqli(DBHST, DBUSR, DBPSWD, DBNM);
	$mysqli->set_charset("utf8");
	$mysqli->query("set session wait_timeout=28800;");
	$mysqli->query("SET GLOBAL time_zone = 'Europe/Moscow';");
	$mysqli->query("SET time_zone = 'Europe/Moscow';");
	
	
	
/*
 * 
 * calculate and update all values in a base table for all current users
 * with calculations related on recent data
 * 
 * 
 * */
function updateIN_DB($mysqli, $INDEX_NO, $REPORT_DATE, $CURRENCY_POLICY, $INDEX_LEVEL, $arrayOfCurrencyValues){
	$selQuery="SELECT * FROM general_data ".
		"WHERE ".
		"INDEX_NO=\"".$mysqli->real_escape_string($INDEX_NO)."\"";
		
	echo $arrayOfCurrencyValues[0]['CURRENCY_VALUE']."\n";
	echo $arrayOfCurrencyValues[0]['CURRENCY_POLICY']."\n";
			
	$resultSelQuery=$mysqli->query($selQuery);
	if(!$resultSelQuery){die("mysql error".$mysqli->error);}
	if(mysqli_num_rows($resultSelQuery)>0){

		while($rowSel=$resultSelQuery->fetch_assoc()){
			
			$INDEX_STRIKE_VALUE=$rowSel['INDEX_STRIKE_VALUE'];
		
			$INDEX_GROWTH=$INDEX_LEVEL/$INDEX_STRIKE_VALUE;
			
			$CURRENCY_GROWTH=$arrayOfCurrencyValues[0]['CURRENCY_VALUE'] / $rowSel['INITIAL_CURRENCY_RATE'];
			
			if((double)$CURRENCY_GROWTH>(double)$rowSel['INDEX_STRIKE_VALUE']){
				$CURRENCY_GROWTH_SIGNED="+".$CURRENCY_GROWTH;
			}else{
				$CURRENCY_GROWTH_SIGNED="-".number_format($CURRENCY_GROWTH, 2, ',', '');
			}
			
			//calculate INVESTMENT_GROWTH
			$INVESTMENT_GROWTH=$rowSel['PARTICIPATION_RATE'] * $CURRENCY_GROWTH * $INDEX_GROWTH * 100;
			
			//calculate INVESTMENT_INCOME
			$INVESTMENT_INCOME = ( $rowSel['GUARANTEED_VALUE'] * $INVESTMENT_GROWTH ) - $rowSel['GUARANTEED_VALUE'];
			if($INVESTMENT_INCOME<=0){$INVESTMENT_INCOME=0;}


			//UPDATE records with new data and calculation related on old data
			$updQuery="UPDATE general_data SET ".
			
			"INDEX_LEVEL=".str_replace(",",".",$mysqli->real_escape_string($INDEX_LEVEL)).",".
			"INDEX_LEVEL_RND=ROUND(".$mysqli->real_escape_string(str_replace(",",".",$INDEX_LEVEL))."),".
			
			"CURRENCY_DATE=STR_TO_DATE('".$arrayOfCurrencyValues[0]['CURRENCY_DATE']."','%d.%m.%Y'),".
			
			"CURRENCY_VALUE=".$mysqli->real_escape_string(str_replace(",",".",$arrayOfCurrencyValues[0]['CURRENCY_VALUE'])).",".
			
			"CURRENCY_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$CURRENCY_GROWTH)).",".
			"CURRENCY_GROWTH_SIGNED=\"".$mysqli->real_escape_string($CURRENCY_GROWTH_SIGNED)."\",".
			
			"INDEX_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$INDEX_GROWTH)).",".
			
			"INVESTMENT_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$INVESTMENT_GROWTH)).",".
			"INVESTMENT_INCOME=".$mysqli->real_escape_string(str_replace(",",".",$INVESTMENT_INCOME)).",".
			
			"REPORT_DATE=NOW()".
			
			" WHERE ".
			" id=\"".$mysqli->real_escape_string($rowSel['id'])."\"";
			//echo $updQuery."\n";exit;
			echo "updated id ".$rowSel['id']."\n";
			$resultUPDQuery=$mysqli->query($updQuery);
			if(!$resultUPDQuery){die("mysql UPD. error, updQuery : \n".$updQuery."\n".$mysqli->error);}
		
		}
		
	}else{
		die("data related on xlsx are not found in the DB");
	}
}

/*
 * 
 * 
 * update cryptocurrency info via api of cbr.ru
 * 
 * 
 * */
function getCurrentCurrencyValues($currencyCode="USD"){
	
	$arrayOfCurrencyValues=[];
	
	$xml=simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
	
	$i=0;
	foreach($xml->Valute as $items){
		if($items->CharCode==$currencyCode){
			$CURRENCY_VALUE=$items->Value;
		}
	}
	
	$arrayOfCurrencyValues[]=array(
					'CURRENCY_DATE'=>$xml['Date'],
					'CURRENCY_POLICY'=>'RUB',
					'CURRENCY_VALUE'=>$CURRENCY_VALUE
					);
	
	return $arrayOfCurrencyValues;
	
}




function updateIN_DB_main_table($mysqli,$POLICY_ID, $POLICY_NO, 
						$STATUS, $CURRENCY, $CURRENCY_POLICY, $SEX_TEXT, $CLIENT_NAME, 
						$REPORT_DATE, $INSR_BEGIN, $INSR_END, $INDEX_NO, $INDEX_LEVEL, 
						$INDEX_LEVEL_RND, $INDEX_LEVEL_TEXT, $INDEX_GROWTH, $INDEX_GROWTH_SIGNED, 
						$CURRENCY_DATE, $CURRENCY_VALUE, $CURRENCY_GROWTH, $CURRENCY_GROWTH_SIGNED, 
						$SURRENDER_VALUE, $ORIG_SURRENDER_VALUE, $PARTICIPATION_RATE, $PARTICIPATION_PERCENTAGE, 
						$INVESTMENT_GROWTH, $INVESTMENT_INCOME, $STRATEGY_NAME, $PREMIUM, $INSURANCE_PROGRAM, 
						$INVESTMENT_DATE, $INDEX_STRIKE_VALUE, $INDEX_STRIKE_RND, $INDEX_STRIKE_TEXT, 
						$INITIAL_CURRENCY_RATE, $INDEX_DESCR, $GUARANTEED_VALUE, $INDEX_MIN_LEVEL, 
						$FILE_LABEL, $REAL_REPORT_DATE, $INSR_BEGIN_MONTH, $INSR_END_MONTH, $GRAPH_END_MONTH, 
						$GRAPH_START_MONTH){
	$selQuery="SELECT * FROM general_data ".
		"WHERE ".
		"POLICY_ID=".$mysqli->real_escape_string($POLICY_ID).
			" AND ".
		"POLICY_NO=\"".$mysqli->real_escape_string($POLICY_NO)."\"".
			" AND ".
		"REPORT_DATE="."\"".$mysqli->real_escape_string($REPORT_DATE)."\"";
	$resultSelQuery=$mysqli->query($selQuery);
	if(!$resultSelQuery){die("mysql error".$mysqli->error);}
	if(mysqli_num_rows($resultSelQuery)==0){//insert if there are no records with this id and report date
		$insQuery="INSERT INTO general_data SET ".
		"POLICY_ID=".$mysqli->real_escape_string($POLICY_ID).",".
		"POLICY_NO=\"".$mysqli->real_escape_string($POLICY_NO)."\",".
		"STATUS=\"".$mysqli->real_escape_string($STATUS)."\",". 
		"CURRENCY=\"".$mysqli->real_escape_string($CURRENCY)."\",". 
		"CURRENCY_POLICY=\"".$mysqli->real_escape_string($CURRENCY_POLICY)."\",". 
		"SEX_TEXT=\"".$mysqli->real_escape_string($SEX_TEXT)."\",". 
		"CLIENT_NAME=\"".$mysqli->real_escape_string($CLIENT_NAME)."\",". 
		"REPORT_DATE=STR_TO_DATE('".$mysqli->real_escape_string($REPORT_DATE)."','%d.%m.%Y'),". 
		"INSR_BEGIN=STR_TO_DATE('".$mysqli->real_escape_string($INSR_BEGIN)."','%d.%m.%Y'),". 
		"INSR_END=STR_TO_DATE('".$mysqli->real_escape_string($INSR_END)."','%d.%m.%Y'),". 
		"INDEX_NO=\"".$mysqli->real_escape_string($INDEX_NO)."\",". 
		"INDEX_LEVEL=".$mysqli->real_escape_string(str_replace(",",".",$INDEX_LEVEL)).",". 
		"INDEX_LEVEL_RND=".$mysqli->real_escape_string($INDEX_LEVEL_RND).",". 
		"INDEX_LEVEL_TEXT=\"".$mysqli->real_escape_string($INDEX_LEVEL_TEXT)."\",". 
		"INDEX_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$INDEX_GROWTH)).",". 
		"INDEX_GROWTH_SIGNED=\"".$mysqli->real_escape_string($INDEX_GROWTH_SIGNED)."\",". 
		"CURRENCY_DATE=STR_TO_DATE('".$mysqli->real_escape_string($CURRENCY_DATE)."','%d.%m.%Y'),". 
		"CURRENCY_VALUE=".$mysqli->real_escape_string(str_replace(",",".",$CURRENCY_VALUE)).",". 
		"CURRENCY_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$CURRENCY_GROWTH)).",". 
		"CURRENCY_GROWTH_SIGNED=\"".$mysqli->real_escape_string($CURRENCY_GROWTH_SIGNED)."\",". 
		"SURRENDER_VALUE=".$mysqli->real_escape_string(str_replace(",",".",$SURRENDER_VALUE)).",". 
		"ORIG_SURRENDER_VALUE=".$mysqli->real_escape_string($ORIG_SURRENDER_VALUE).",". 
		"PARTICIPATION_RATE=".$mysqli->real_escape_string(str_replace(",",".",$PARTICIPATION_RATE)).",". 
		"PARTICIPATION_PERCENTAGE=".$mysqli->real_escape_string($PARTICIPATION_PERCENTAGE).",". 
		"INVESTMENT_GROWTH=".$mysqli->real_escape_string(str_replace(",",".",$INVESTMENT_GROWTH)).",". 
		"INVESTMENT_INCOME=".$mysqli->real_escape_string(str_replace(",",".",$INVESTMENT_INCOME)).",". 
		"STRATEGY_NAME=\"".$mysqli->real_escape_string($STRATEGY_NAME)."\",". 
		"PREMIUM=".$mysqli->real_escape_string($PREMIUM).",". 
		"INSURANCE_PROGRAM=\"".$mysqli->real_escape_string($INSURANCE_PROGRAM)."\",".
		"INVESTMENT_DATE=STR_TO_DATE('".$mysqli->real_escape_string($INVESTMENT_DATE)."','%d.%m.%Y'),". 
		"INDEX_STRIKE_VALUE=".$mysqli->real_escape_string(str_replace(",",".",$INDEX_STRIKE_VALUE)).",". 
		"INDEX_STRIKE_RND=".$mysqli->real_escape_string($INDEX_STRIKE_RND).",". 
		"INDEX_STRIKE_TEXT=\"".$mysqli->real_escape_string($INDEX_STRIKE_TEXT)."\",". 
		"INITIAL_CURRENCY_RATE=".$mysqli->real_escape_string(str_replace(",",".",$INITIAL_CURRENCY_RATE)).",". 
		"INDEX_DESCR=\"".$mysqli->real_escape_string($INDEX_DESCR)."\",". 
		"GUARANTEED_VALUE=".$mysqli->real_escape_string($GUARANTEED_VALUE).",". 
		"INDEX_MIN_LEVEL=".$mysqli->real_escape_string(str_replace(",",".",$INDEX_MIN_LEVEL)).",". 
		"FILE_LABEL=\"".$mysqli->real_escape_string($FILE_LABEL)."\",". 
		"REAL_REPORT_DATE=STR_TO_DATE('".$mysqli->real_escape_string($REAL_REPORT_DATE)."','%d.%m.%Y'),". 
		"INSR_BEGIN_MONTH=\"".$mysqli->real_escape_string($INSR_BEGIN_MONTH)."\",". 
		"INSR_END_MONTH=\"".$mysqli->real_escape_string($INSR_END_MONTH)."\",". 
		"GRAPH_END_MONTH=\"".$mysqli->real_escape_string($GRAPH_END_MONTH)."\",". 
		"GRAPH_START_MONTH=\"".$mysqli->real_escape_string($GRAPH_START_MONTH)."\"";
		$resultINSQuery=$mysqli->query($insQuery);
		if(!$resultINSQuery){die("mysql INS. error".$mysqli->error);}
	}
}
?>
